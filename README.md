Zadania na laboratoria
====================
## Zad. 1

>Dla podanych z wejścia: naturalnej n >= 1, końców a i b przedziału [a, b] oraz zestawu wartości y_i, 
>0 <= i <= n, przygotować węzły równoodległe x_i = a + ih, 0 <= i <= n, gdzie h = (b-a)/(n), 
>i wypisać w postaci ogólnej wielomianu P stopnia n taki, że P(x_i)=y_i, 0 <= i <=leq n. 
>Współczynniki wielomianu wyznaczyć przez rozwiązanie odpowiedniego układu równań metodą eliminacji.
>Powtarzać proces dla nowych danych tyle razy, ile zechce użytkownik.

[Dokumentacja](https://www.overleaf.com/3535434grtxrj#/10023877/)


## Zad. 2

>Przetestować działanie metody Newtona dla układu

>x^2 + 4y^2 - 4 = 0

>x^2 - 2x - y + 1 = 0

>przy różnych wczytanych z wejścia punktach startowych (x_0, y_0)

[Dokumentacja](https://www.overleaf.com/3821111jncdqh#/10984742/)


## Zad. 3

>Wczytać liczbę naturalną n >= 1 i liczbę rzeczywistą a > 0. Zagadnienie różniczkowe

>y'(x) = f(x, y(x)), y(1) = 0,

>gdzie f(x, y) = sqrt{y+x}+x-1 rozważyć na przedziale [1, a] z krokiem h = (a-1)\(n) metodą Eulera

>oraz zmodyfikowaną metodą Eulera, zwaną też metodą punktu środkowego. 

>Wyniki porównywać z rozwiązaniem dokładnym y(x) = x^2 - x przez wyznaczenie błędu 

>maksymalnego każdej z metod.

[Dokumentacja](https://www.overleaf.com/4012398rtrdsm#/11687886/)