#include<stdio.h>
#include<stdlib.h>
#include<math.h>

//-------------------------------Dane do zmiany w kodzie----------
#define y0 0.0
/***
 * funkcja: podstawienie x do funkcji
*/
double funkcja(double x, double y){
	return
	sqrt(y + x) + x - 1;
}

/***
 * rozwDokladnie: podstawienie x do rozwiazania dokladnego
*/
double rozwDokladnie(double x){
	return x*x - x;
}

//------------------------------------------------------------------------


double bezwzgledna(double number){
	if (number<0) number = -number;
	return number;
}


double euler(double poprzednie, double h, int k ){
	return poprzednie + h*funkcja(k*h + 1, poprzednie);
}


double zmodyfikowanyEuler(double poprzednie, double h, int k){
	return poprzednie + h*funkcja( k*h + 1 + 0.5*h, poprzednie + 0.5*h*funkcja(k*h + 1, poprzednie) );
}


void wyniki(double h, double n, int ilosc ){
	int i, pomKroki;
	double poprzednieE = y0, poprzednieZ = y0, maxRoznicaE = 0.0, maxRoznicaZ = 0.0, pom;
	pomKroki = n/ilosc;
	printf("\n\n     k      |     xk     |    Euler   | zmod.Euler | rozwiazanie dokladne");
	printf("\n------------|------------|------------|------------|----------------------");
	for (i = 0; i < n; i++){

		poprzednieE = euler(poprzednieE, h, i);
		poprzednieZ = zmodyfikowanyEuler(poprzednieZ, h, i);
		if((i+1)%pomKroki == 0){
			printf("\n     %d      |  %lf  |  %lf  |  %lf  |  %lf", i+1, (i+1)*h+1, poprzednieE, poprzednieZ, rozwDokladnie((i+1)*h+1) );
			printf("\n------------|------------|------------|------------|----------------------");
		}
		pom = poprzednieE - rozwDokladnie((i+1)*h+1);
		if (maxRoznicaE <= bezwzgledna( pom ) ) maxRoznicaE = bezwzgledna( pom );
		pom =  poprzednieZ - rozwDokladnie((i+1)*h+1);
		if (maxRoznicaZ <= bezwzgledna( pom ) ) maxRoznicaZ = bezwzgledna( pom );
	}

	printf("\n\nMaksymalny blad:\n\t%lf - Euler\n\t%lf - Zmodyfikowany Euler", maxRoznicaE, maxRoznicaZ );
}


int main(){
	double a, n, pomd;
	int ilosc;

	printf("\nWczytywanie konca przedzialu [1, a]. a>1 ");
	printf("\nPrzykladowe a: 1.34");
	printf("\nPodaj a: ");
	scanf("%lf", &a);

    while(a <= 1){
    printf("\nPodano bledne a, musi byc wieksze od 1!\nPodaj jeszcze raz koniec przedzialu: ");
		printf("\nPrzykladowe a: 1.34");
		printf("\nPodaj a: ");
		scanf("%lf", &a);
    }

	printf("\nWczytywanie ilosci krokow w przedziale n. n>0 (automatycznie zaokragla w dol do liczby calkowitej) ");
	printf("\nPodaj n: ");
	scanf("%lf", &n);

    while(n<1){
        printf("\nPodano bledne n, musi byc wieksze od 0!\nPodaj jeszcze raz ilosc krokow: ");
		printf("\nPodaj n: ");
		scanf("%lf", &n);
    }

	printf("\na = %lf", a);
	printf("\nDlugosc kroku: h = (a-1)/n = %lf", (a-1)/n);

	//euler(a/n, n);
	//zmodyfikowanyEuler(a/n, n);
	printf("\nSpodziewana ilosc wynikow: %lf\nNapisz ile ich chcesz zobaczyc (ilosc, automatycznie zaokragla w dol do liczby calkowitej)", n);
	printf("\nPodaj ilosc: ");
	scanf("%lf", &pomd);

    while(pomd<1 || pomd>n){
        printf("\nPodano bledna ilosc, musi byc wieksze od 0, ale nie wieksza niz %lf!\nPodaj jeszcze raz ilosc: ", n);
		printf("\nPodaj ilosc: ");
		scanf("%lf", &pomd);
    }
	ilosc = pomd;
	wyniki((a-1)/n, n, ilosc);
	printf("\n");
	return 0;

}
