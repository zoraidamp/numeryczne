//pamietac o dodaniu "-lm" przy kompilacji
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

//---Dane wpisane na sztywno do kodu:
//   max krokow
//   delte
//   2 wzory (gorna i dolna)
//   pochodne wzorow (po zmiennej x lub y )
#define maxKrokow 20
#define delta 1.0

double wzorA (double x, double y){
	return ( x*x + 4 * y * y - 4);
}

double wzorB (double x, double y){
	return ( x*x - 2 * x - y + 1);
}

double pochodnaApoX (double x, double y){
	return ( 2*x );
}

double pochodnaApoY (double x, double y){
	return ( 8*y );
}

double pochodnaBpoX (double x, double y){
	return ( 2*x-2 );
}

double pochodnaBpoY (double x, double y){
	return ( -1 );
}
//---------------------------


typedef struct MacierzM {
  double a;
	double b;
} MacierzM;


typedef struct MacierzKw {
  double a;
	double b;
	double c;
	double d;
} MacierzKw;

MacierzKw *mnMacierzKwNr (MacierzKw *macierz, double Nr){
	MacierzKw *wynik = malloc (sizeof(MacierzKw));
	wynik->a = Nr*(macierz->a);
	wynik->b = Nr*(macierz->b);
	wynik->c = Nr*(macierz->c);
	wynik->d = Nr*(macierz->d);
	free(macierz);
	return wynik;
}

	MacierzKw *macierzKwOdwrotna (MacierzKw *macierz){
	double pomNr;
	MacierzKw *wynik = malloc (sizeof(MacierzKw));
	pomNr = (macierz->a)*(macierz->d);
	pomNr -= (macierz->b)*(macierz->c);
	if( pomNr == 0 ){
		printf("\nNie da sie jednym sposobem obliczyc ");
		printf("macierzy odwrotnej!");
		exit(0);
	}
	pomNr = 1 / pomNr;

	wynik->a = macierz->d;
	wynik->b = -(macierz->b);
	wynik->c = -(macierz->c);
	wynik->d = macierz->a;

	wynik = mnMacierzKwNr (wynik, pomNr);
	free(macierz);
	return wynik;
}

MacierzM *mnMacierzKwMacierzM (MacierzKw *macierzA, MacierzM *macierzB){
	MacierzM *wynik = malloc (sizeof(MacierzM));
	wynik->a = (macierzA->a)*(macierzB->a);
	wynik->a += (macierzA->b)*(macierzB->b);
	wynik->b = (macierzA->c)*(macierzB->a)+(macierzA->d)*(macierzB->b);
	free(macierzA);
	free(macierzB);
	return wynik;
}

MacierzM *odjMacierzMacierzM (MacierzM *macierzA, MacierzM *macierzB){
	MacierzM *wynik = malloc (sizeof(MacierzM));
	wynik->a = (macierzA->a)-(macierzB->a);
	wynik->b = (macierzA->b)-(macierzB->b);
	free(macierzA);
	free(macierzB);
	return wynik;
}

/***
 * obliczKrok(): funkcja, ktora liczy dany krok
 * za pomoca [x_n, y_n] oblicza [x_(n+1), y_(n+1)]
*/
MacierzM *obliczKrok (MacierzM *macierzP){
	MacierzKw *macierzA = malloc (sizeof(MacierzKw));
	MacierzM *macierzB = malloc (sizeof(MacierzM));
	MacierzM *wynik = malloc (sizeof(MacierzM));

	macierzA->a = pochodnaApoX( (macierzP->a), (macierzP->b) );
	macierzA->b = pochodnaApoY( (macierzP->a), (macierzP->b) );
	macierzA->c = pochodnaBpoX( (macierzP->a), (macierzP->b) );
	macierzA->d = pochodnaBpoY( (macierzP->a), (macierzP->b) );
	macierzA = macierzKwOdwrotna(macierzA);

	macierzB->a = wzorA( (macierzP->a), (macierzP->b) );
	macierzB->b = wzorB( (macierzP->a), (macierzP->b) );

	macierzB = mnMacierzKwMacierzM(macierzA, macierzB);

	wynik = odjMacierzMacierzM(macierzP, macierzB);
	free(macierzP);
	free(macierzA);
	free(macierzB);
	return wynik;
}

MacierzM *wczytywaniePunktuS(){
	MacierzM *punktS = malloc (sizeof(MacierzM));
	double x0, y0;

	puts("\nPodaj x_0: ");
	scanf("%lf", &x0);
	puts("\nPodaj y_0: ");
	scanf("%lf", &y0);

	punktS->a=x0;
	punktS->b=y0;

	return punktS;
}

/***
 * rozwiazZadanie(): funkcja, ktora  przy wykorzystaniu podfukncji pobiera dane i wylicza kolejne kroki
*/
void rozwiazZadanie(){
	int nrKrok = 0;
	double roznicaX, roznicaY;
	MacierzM *m, *poprzM;

	poprzM=wczytywaniePunktuS();
	printf("\n delta = %lf, max krokow = %d", delta, maxKrokow);
	printf("\n krok(i) | [x_i, y_i ]| roznica");
	printf("\n 0       | [ %0.4lf, %0.4lf]", poprzM->a, poprzM->b );

	nrKrok++;
	m = obliczKrok(poprzM);
	roznicaX = fabs( (m->a) - (poprzM->a) );
	roznicaY = fabs( (m->b) - (poprzM->b) );
	printf("\n 1       | [ %0.4lf, %0.4lf] | [%lf, %lf]", m->a, m->b, roznicaX, roznicaY);

	while( ( delta <= roznicaX || delta <= roznicaY ) &&  nrKrok<maxKrokow){
		poprzM = m;
		nrKrok++;
		m = obliczKrok(poprzM);
		roznicaX = fabs( (m->a) - (poprzM->a) );
		roznicaY = fabs( (m->b) - (poprzM->b) );
		printf("\n %d       | [ %0.4lf, %0.4lf] | [%lf, %lf]", nrKrok, m->a, m->b, roznicaX, roznicaY);
	}

	if (delta > roznicaX && delta > roznicaY)
		printf("\n \n----roznica[%lf, %lf] \njest mniejsza niz delta(%lf)", roznicaX, roznicaY, delta);
	else
		printf("\n \n----wykonano maksymalnie duzo krokow(%d), \n----a roznica[%lf, %lf] jest wciaz \nmniejsza/rowna delta(%lf)", nrKrok,roznicaX, roznicaY, delta);
	free(m);
	free(poprzM);
}



int main(){

	rozwiazZadanie();
	printf("\n");
	return 0;
}
