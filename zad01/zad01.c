//pamietac o dodaniu "-lm" przy kompilacji
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

/***
 * wczytywaniePrzedzialu(); funkcja wczytujaca naturalna liczbę n, końce przedziału start i koniec
*/
void wczytywaniePrzedzialu(int output[3]){
	int n, start, koniec;
	
	puts("\nPodaj n: ");
	scanf("%d", &n);
    while(n<=0){
        printf("\nPodano bledne n, musi byc wieksze od 0!\nPodaj jeszcze raz liczbe podzialu przedzialu: ");
		puts("\nPodaj n: ");
		scanf("%d", &n);
    }
	
	puts("\nPodaj poczatek przedzialu: ");
	scanf("%d", &start);
	puts("\nPodaj koniec przedzialu: ");
	scanf("%d", &koniec);	
	while(start>=koniec){
        printf("\nPodano bledne przedzialy, poczatek musi byc mniejszy od konca!\nPodaj jeszcze raz konce przedzialu: ");
		puts("\nPodaj poczatek przedzialu: ");
		scanf("%d", &start);
		puts("\nPodaj koniec przedzialu: ");
		scanf("%d", &koniec);	
    }
	output[0]=n;
	output[1]=start;
	output[2]=koniec;
}	

/***
 * liczenieWezlow(); funkcja, ktora na postawie przedzialu (start, koniec) i n wylicza wezly (x[]) 
 * i wczytuje ich wartosci y[]
 */
void liczenieWezlow(double x[], double y[], int n, int start, int koniec){
	int i;
	double h=(koniec - start);
	
	h /= n;
	//puts("\nPodaj n: ");
	//scanf("%d", &n);
	
	for(i=0; i<=n; i++){
		x[i]=start + i*h;
    }
	
	printf("\nPodaj wartosci wezlow:");
    for(i=0; i<=n; i++){
        printf("\nf(%lf)=y_%d: ", x[i],i);
		scanf("%lf", &y[i]);
    }
}

/***
 * tworzenieMacierzy(): funkcja, ktora na postawie wezlow x[] tworzy macierz m[][]
 * m[y][x]
 */
void tworzenieMacierzy(int n, double m[n+1][n+1], double x[]){
	int i, j;
	
	for(i=0; i<=n; i++){
		//printf("\n");
		for(j=0; j<=n; j++){
			m[i][j]=pow(x[i], j);
			//printf(" %lf",m[i][j]);
		}
    }

}
	
/***
 * zamienNaSchodkowa(); funkcja, ktora przeksztalca podana macierz na macierz schodkowa gorna
*/ 
void zamienNaSchodkowa(int n, double m[n+1][n+1], double y[]){
	int wiersz, i, j;
	double ile;
	for(wiersz=0; wiersz<n; wiersz++){
		for(i=wiersz+1; i<=n; i++){
			ile= -( m[i][wiersz] / m[wiersz][wiersz] );
			for(j=wiersz; j<=n; j++){
				m[i][j]+=m[wiersz][j]*ile;
			}
			y[i]+=y[wiersz]*ile;
			//printMY(n, m, y);
			if( m[i][wiersz+1]!=1 ){
				//printf("\n**m[%d][%d]: %lf", i, wiersz+1, m[i][wiersz+1]);
				ile = 1.00/( m[i][wiersz+1] );
				for(j=wiersz+1; j<=n; j++){
					m[i][j]*=ile;
				}
				y[i]*=ile;
				//printMY(n, m, y);
			}
		}
	}
	
	//printMY(n, m, y);

}

/***
 * liczRozwiazanie(): funkcja, ktora na podstawie macierzy schodkowej m 
 * wylicza wpolczynniki a[] szukanego wielomianu
*/ 
void liczRozwiazanie(int n, double m[n+1][n+1], double y[], double a[]){
	int i, j;
	
	for(i=n; i>=0; i--){
		a[i]=y[i];
		for(j=i+1; j<=n; j++){
			a[i]-=m[i][j]*a[j];
		}
    }
	
	//printf("\n");
	//for(i=0; i<=n; i++){
	//	printf("\na[%d] = %lf", i, a[i]);
    //}

}

/***
 * drukujWielomian(): funkcja, ktora na podstawie listy wspolczynnikow a[] drukuje wielomian
*/
void drukujWielomian(int n, double a[]){
	int i, pom = 0;
	printf("\nSzukany wielomian:");
	for(i = 0; i <= n; i++){
		if(pom == 0 && a[i] != 0){
			printf("\n%lf", a[i]);
			pom = 1;
		}	
		else{
			if(a[i] != 0){
				if(a[i] > 0) printf(" + %lf", a[i]);
				else printf(" %lf", a[i]);
			
				if(i == 1) printf("x");
				else printf("x^%d", i);
			}
		}
    }

}

/***
 * rozwiazZadanie(): funkcja, ktora  przy wykorzystaniu podfukncji pobiera dane i wylicza wielomian
*/
void rozwiazZadanie(){
	int output[3], n, start, koniec;
	
	wczytywaniePrzedzialu(output);
	n=output[0];
	start=output[1];
	koniec=output[2];

	double x[n+1],y[n+1], m[n+1][n+1], a[n+1];
	liczenieWezlow(x, y, n,start,koniec);
	tworzenieMacierzy(n, m, x);
	zamienNaSchodkowa(n, m, y);	
	liczRozwiazanie(n, m, y, a);
	drukujWielomian(n, a);
}


int main(){
	
	int stop=0;
	char odp = 'T';

	while(stop == 0){

		switch(odp) {
			case 'T' :
				rozwiazZadanie();
				printf("\nCzy chcesz obliczyc kolejny wielomian? (T/N)\nOdp: " );
				getchar();
				odp=getchar();				
				break;
			case 't' :
				rozwiazZadanie();
				printf("\nCzy chcesz obliczyc kolejny wielomian? (T/N)\nOdp: " );
				getchar();
				odp=getchar();				
				break;	  
			case 'N' :
				printf("\nProgram zostanie wylaczony." );
				stop = 1;
				break;
			case 'n' :
				printf("\nProgram zostanie wylaczony." );
				stop = 1;
				break;
			default :
				printf("\nNieznana odpowiedz. Program zostanie wylaczony." );
				stop = 1;
		}	 
	}
	
	return 0;
}